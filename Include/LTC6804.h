#ifndef LTC6804_H
#define LTC6804_H


/*Dependencies------------------------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "SPI.h"
#include "stm32f4xx_rcc.h"
#include "FreeRTOS.h"
#include "task.h"

#define TSLEEP 1800
#define TWAKE 300
#define TREADY 10

/*Function Prototypes-----------------------------------------------------------------------*/
void LTC6804_init(void);
uint8_t LTC6804_voltage(void);
uint8_t LTC6804_temperature(void);
void wakeUp(void);


#endif
