#ifndef SPI_H
#define SPI_H


/*Dependencies------------------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

typedef enum {
	SPI_1 = 0,	//SPI_slave
	SPI_2 = 1,	//SPI_flash
	SPI_3 = 2,

}SPI_t;

/*Function Prototypes-----------------------------------------------------------------------*/
void SPI_init(SPI_t SPI_number);
void SPI_write(SPI_TypeDef * SPIx, uint8_t message);
uint8_t SPI_read(SPI_TypeDef * SPIx);
void SPI_CsEnable(SPI_TypeDef * SPIx);
void SPI_CsDisable(SPI_TypeDef * SPIx);
void SPI_Enable(SPI_TypeDef * SPIx);


#endif
