
/*Dependencies------------------------------------------------------------------------------*/
#include "SPI.h"

/*SPI periperal--------------------------------*/
SPI_TypeDef* spiName[2] = {SPI2,SPI1};



/*Function Prototypes-----------------------------------------------------------------------*/
void SPI_settings(SPI_TypeDef * SPIx);


/*Activate SPI Chip select------------------------------------------------------------------*/
#define _ACTIVATE_SPI_CSS(PORT,PIN) (PORT->BSRRL |= PIN)

/*Deactivate SPI Chip select----------------------------------------------------------------*/
#define _DEACTIVATE_SPI_CSS(PORT,PIN) (PORT->BSRRH |= PIN)

/*Initializing for SPI1, SPI2, SPI3 -- SPI3 is not operational*/
/*Program-----------------------------------------------------------------------------------*/
void SPI_init(SPI_t SPI_number) {
	/*Initialize periperal clocks for SPI and Port  */
	if (SPI_number < 1)
	{
	RCC->APB2ENR |= RCC_APB2LPENR_SPI1LPEN;
	RCC->AHB1ENR |= 0x01;
	}
	else
	{
	RCC->APB1ENR |= (1<<(13+SPI_number));
	RCC->AHB1ENR |= (1<<2)|(0x02>>(SPI_number-1));
	}
	
	/*SPI1------------------------------------------------------------------------------------*/
	if (SPI_number == 0)
	{
	/*Initialize GPIO pin PA4, PA5, PA6, PA7*/
	GPIOA->MODER |= GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1;
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_4 | GPIO_OTYPER_OT_5 | GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7);
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR4 | GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7);
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR4_1 | GPIO_OSPEEDER_OSPEEDR5_1 | GPIO_OSPEEDER_OSPEEDR6_1 | GPIO_OSPEEDER_OSPEEDR7_1;   
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR4 | GPIO_PUPDR_PUPDR5 | GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR7);
	
	/*Adress PA5, PA6, PA7 to SPI module*/
	GPIOA->AFR[0] &= ~(0xFFF0U<<16);
	GPIOA->AFR[0] |= (0x5550U<<16);
		
	/*Initialize settings for SPI1*/	
	SPI_settings(SPI1);
	}
	/*SPI2------------------------------------------------------------------------------------*/
	else if (SPI_number == 1)
	{
	/*Initialize GPIO pin PC2, PC3, PB10, PB12*/
	GPIOC->MODER |= GPIO_MODER_MODER2_1 | GPIO_MODER_MODER3_1;
	GPIOB->MODER |= GPIO_MODER_MODER10_1 | GPIO_MODER_MODER12_0;
		
	GPIOC->OTYPER &= ~(GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_3);
	GPIOB->OTYPER &= ~(GPIO_OTYPER_OT_10 | GPIO_OTYPER_OT_12);
		
	GPIOC->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR3 );
	GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR3_1;
	GPIOB->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR10 | GPIO_OSPEEDER_OSPEEDR12);
	GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR10_1 | GPIO_OSPEEDER_OSPEEDR12_1);
		
	GPIOC->PUPDR &= ~(GPIO_PUPDR_PUPDR2 | GPIO_PUPDR_PUPDR3);
	GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR10 | GPIO_PUPDR_PUPDR12);
		
	/*Adress PB10, PC2 and PC3 to SPI module*/
	GPIOC->AFR[0] &= ~(0xFF00U);
	GPIOC->AFR[0] |= (0x5500U);
	GPIOB->AFR[1] &= ~(0x0F00U);
	GPIOB->AFR[1] |= (0x0500U);
		
	/*Initialize settings for SPI2*/
	SPI_settings(SPI2);
	}
	/*SPI3------------------------------------------------------------------------------------*/
	else
	{
	/*Initialize GPIO pin PC10, PC11, PC12, PA15*/
	GPIOC->MODER &= ~(GPIO_MODER_MODER10 | GPIO_MODER_MODER11 | GPIO_MODER_MODER12);
	GPIOC->MODER |= GPIO_MODER_MODER10_1 | GPIO_MODER_MODER11_1 | GPIO_MODER_MODER12_1 ;
	GPIOC->MODER &= ~(GPIO_MODER_MODER15);
	GPIOA->MODER |= GPIO_MODER_MODER15_0;
		
	GPIOC->OTYPER &= ~(GPIO_OTYPER_OT_10 | GPIO_OTYPER_OT_11 | GPIO_OTYPER_OT_12);
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_15);
		
	GPIOC->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR10 | GPIO_OSPEEDER_OSPEEDR11 | GPIO_OSPEEDER_OSPEEDR12);
	GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10_1 | GPIO_OSPEEDER_OSPEEDR11_1 | GPIO_OSPEEDER_OSPEEDR12_1;
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDER_OSPEEDR15);
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_1;
		
	GPIOC->PUPDR &= ~(GPIO_PUPDR_PUPDR10 | GPIO_PUPDR_PUPDR11 | GPIO_PUPDR_PUPDR12);
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR15);
	/*Adress PC10, PC11 and PC12 to SPI module*/
	GPIOC->AFR[1] &= ~(0xFFF0U<<4);
	GPIOC->AFR[1] |= (0x6660U<<4);
		
	/*Initialize settings for SPI3*/	
	SPI_settings(SPI3);
	}
}

/*Write to SPIx-----------------------------------------------------------------------------*/
void SPI_write(SPI_TypeDef * SPIx, uint8_t message)
	{
	SPIx->DR = message;
	while( !(SPIx->SR & SPI_I2S_FLAG_TXE) );
	}
	
/*Read from SPIx----------------------------------------------------------------------------*/	
uint8_t SPI_read(SPI_TypeDef * SPIx)
	{
	SPI_write(SPI1,0xFF);
	while( !(SPIx->SR & SPI_I2S_FLAG_RXNE) ); // wait until receive complete
	while( SPIx->SR & SPI_I2S_FLAG_BSY ); // wait until SPI is not busy anymore
	return SPIx->DR; // return received data from SPI data register
	}	

/*Enable Chip select on SPIx----------------------------------------------------------------*/	
void SPI_CsEnable(SPI_TypeDef * SPIx)
	{	
		if (SPIx == SPI1)
		{
		_ACTIVATE_SPI_CSS(GPIOA,GPIO_Pin_4);
		}
		else if (SPIx == SPI2)
		{
		_ACTIVATE_SPI_CSS(GPIOB,GPIO_Pin_12);
		}
		else//(SPIx == SPI3)
		{
		_ACTIVATE_SPI_CSS(GPIOA,GPIO_Pin_15);
		}
	}

void SPI_Enable(SPI_TypeDef * SPIx)
{
	if((SPIx->CR1 & 0x0100) != 0x0100)
	{
	SPIx->CR1 |= 0x0100;
	}else
	{
	SPIx->CR1 &= 0xFEFF;
	}
}
	
/*Disable Chip select on SPIx---------------------------------------------------------------*/	
void SPI_CsDisable(SPI_TypeDef * SPIx)
	{		
		if (SPIx == SPI1)
		{
		_DEACTIVATE_SPI_CSS(GPIOA,GPIO_Pin_4);
		}
		else if (SPIx == SPI2)
		{
		_DEACTIVATE_SPI_CSS(GPIOB,GPIO_Pin_12);
		}
		else//(SPIx == SPI3)
		{
		_DEACTIVATE_SPI_CSS(GPIOA,GPIO_Pin_15);
		}
		
	}

/*Initialize SPI settings-------------------------------------------------------------------*/		
void SPI_settings(SPI_TypeDef * SPIx){
	/*Initialize SPI CR1 register*/
	uint16_t tmpreg = 0;
	tmpreg = SPIx->CR1;
	tmpreg &= 0x3040;
	tmpreg |=  SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_MSTR | SPI_CR1_BR_1 | SPI_CR1_BR_2 | SPI_CR1_CPOL | SPI_CR1_CPHA;
	SPIx->CR1 |= tmpreg;
	
	 /* Activate the SPI mode (Reset I2SMOD bit in I2SCFGR register) */
  SPIx->I2SCFGR &= (uint16_t)~((uint16_t)SPI_I2SCFGR_I2SMOD);
	
	/*Enable SPI*/
	SPIx->CR1 |=0x0040;
}


	
